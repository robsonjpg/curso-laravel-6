    <!--Pode ter opções de menu, navegações etc 
    pode definir aqui, porém a desvantagem é que teria que repetir pra todas views que tivesse, gestão de produtos, financeira etc...
    pra isso que existe os templates
    para isso tudo ficará lá no app.blade.php-->

    @extends('admin.layouts.app')
    
    <!--
    @ section('title')
    Gestão de Produtos
    @ endsection

    Ao invés do de cima, pode fazer como o de baixo também
    -->

    @section('title', 'Gestão de produtos')<!--Fica o título lá na aba-->


    @section('content') <!--mostra onde vai substituir, e coloca esse esse-->
    <h1>Exibindo os produtosss</h1>
                    <!--Coloca o que quer exibir aqui-->
    
    <!--AULA Listar/Paginar Registros -->
    <a href="{{ route('products.create') }}">Cadastrar</a>
    <hr>
    <!--aqui está fazendo a listagem dos produtos em si, q pegou lá do controller-->
    <table border="1">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Preço</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)
            <tr>
                <td>{{ $product->name }}</td>
                <td>{{ $product->price }}</td>
                <td>
                    <a href="">Detalhes</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>


    <!--PRA COLOCAR PRA PULAR PÁGINA-->
    {!! $products->links() !!}
    
    @endsection
